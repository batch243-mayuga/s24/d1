// console.log("Hello world");

// [Section] Exponent operator

// before ES6
	const firtNum = 8 ** 2;
	console.log(firtNum);

// ES6
	const secondNum = Math.pow(8, 2);
	console.log(secondNum);

// [Section] Template Literals
	/*
		-allows to write strings without using concatenation operator (+)
		-greatly helps with code readability
	*/

	let name = "John";
		// before template literal string
		// we use single or double quote
		let message = 'Hello ' + name + '! Welcome to programming!';
		console.log(message);

		// Using template literal
		// uses backticks(``);

		message = `hello ${name}! Welcome to programming!`
		console.log(message);

		// Template literals allow us to write string with embedded JavaScript

		const interestRate = 0.1;
		const principal = 1000;
		console.log(`The interest on your savings account is: ${interestRate * principal}`);

// [Section] Array Destructuring
	/*
		-allows us to unpack elements in arrays into distinct variables
		-allows us to name array elements with variables instead of using index numbers
		- it will help us with code readability
		-Syntax:
		let/const [variableNameA, variableNameB, ...] = arrayName;
	*/

	const fullName = ["Juan", "Dela", "Cruz"];
	// Before array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

	// Array destructuring

	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(fullName);
	console.log(`Hello ${firstName} ${middleName} ${lastName}! Its nice to meet you.`);

// [Section] Object Destructuring
	/*
		-it allows us to unpack properties of objects into distinct variables
		Syntax:
			let/const { propertyNameA, propertyNameB, propertyNameC, ...} = objectName;
	*/

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}
	// Before the object destructuring
		console.log(person.givenName);
		console.log(person["maidenName"]);
		console.log(person.familyName);
	// Object Destructuring
		let {givenName, maidenName, familyName} = person;
		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);


	function getFullName({givenName, maidenName, familyName}){
		console.log(`${givenName} ${maidenName} ${familyName}`);
	}

	getFullName(person);


// [Section] Arrow Functions
	// Compact alternative syntax to traditional functions.
	// useful for code snippets where creating functions will not be reused in any other portion of the code

	const hello = () => {
		console.log("Hello World!");
	}

	/*const hello = function(){
		console.log("Hello World!");
	}
*/
	hello();

	// before arrow function and template literals
		function printFullName(firstName, middleInitial, lastName){
			console.log(firstName + ' ' + middleInitial + ' '+ lastName);
		}

		printFullName("Chris", "o.", "Mortel");
		// 
		let fName = (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial} ${lastName}`)
		}
		fName("Chris", "o.", "Mortel");

		// Arrow functions with loops

		const student = ["John", "Jane", "Judy"];

		// before arrow function
			// for every element nung student array, makapagconsole.log "studentName is a student."

		function iterate(student){
			console.log(student + " is a student");
		}

		student.forEach(iterate);

		// arrow function

		student.forEach((student)=>{
			console.log(`${student} is a student`)
		})

		// [Section] Implicit Return Statement
			/*
				there are instances when you can omit return statement
				this works because even without return statement Javascript implicitly adds it for the result of the function
			*/

	const add = (x,y) => {
		console.log(x+y);
		return x+y;
	}

	let sum = add(23, 45);
	console.log("This is the sum contain in sum variable");
	console.log(sum);

// 
	const subtract = (x, y) => x-y;

	subtract(10, 5);
	let difference = subtract(10, 5);
	console.log(difference);

	// [Section] Default Function Argument Value
	// provide a default argument value if none is provided when the function is invoked.

	const greet = (name = "User") => {
		return `Goodmorning, ${name}`;
	}

	console.log(greet());

	// [Section] Class-based Object Blueprints

	// Allows us to create or instantiation of objects using classes blueprints

	// it creating a class
		// constructor is a special method of a class for creating/initializing an object for that class
	/*
		Syntax:
		class className{
				constructor (objectValueA, objectValueB, ...){
				this.objectPropertyA = objectValueA;
				this.objectPropertyB = objectValueB;
				}
		}
	*/

	class Car{
		constructor(brand, name, year){
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}

	let car = new Car("Toyota", "Hilux-pickup", "2015");
	console.log(car);

	car.carBrand = "Nissan";
	console.log(car);